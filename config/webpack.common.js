const path = require('path')
const DotEnv = require('dotenv-webpack')
const webpack = require('webpack');
const HTMLwebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const isDevMode = process.env.NODE_ENV !== "production";

module.exports = {
	entry: path.resolve(__dirname, '..', './src/index.tsx'),
	resolve: {
		extensions: ['.ts', '.tsx', '.js']
	},
	module: {
		rules: [
				{
					test: /\.js?$/,
					exclude: /node_modules/,
					use: ['babel-loader']
				},
				{
					test: /\.tsx?$/,
					exclude: /node_modules/,
					use: 'ts-loader',
				},
				{
					test: /\.less$/i,
					exclude: /node_modules/,
					use: [ isDevMode ? 'style-loader' : MiniCssExtractPlugin.loader, 'css-loader', 'less-loader' ]
				},
				{
					test: /\.(woff|woff2|eot|ttf|otf)$/,
					type: 'asset/resource',
					dependency: { not: ['url'] },
				},  
				{
					test: /\.(?:ico|gif|png|jpg|jpeg|svg)$/i,
					type: 'asset/resource'
				}   
			]
    },
		output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, '../build'),
            clean: true,
		},
		plugins: [
			new HTMLwebpackPlugin({
				template: path.resolve(__dirname, '..', './index.html')
			}),
			new DotEnv({ systemvars: true }),
		].concat(isDevMode ? [] : [new MiniCssExtractPlugin()])
}
