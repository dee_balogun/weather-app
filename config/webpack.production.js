const path = require('path')
const { merge } = require('webpack-merge')
const commonWebpackConfig = require('./webpack.common')

module.exports = merge(commonWebpackConfig, {
	mode: 'production',
	devtool: 'source-map',
})
