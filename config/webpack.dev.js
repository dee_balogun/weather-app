const path = require('path')
const { merge } = require('webpack-merge')
const commonWebpackConfig = require('./webpack.common')

module.exports = merge(commonWebpackConfig, {
	mode: 'development',
	devtool: 'inline-source-map',
	devServer: {
		static: '../build',
	  },
})