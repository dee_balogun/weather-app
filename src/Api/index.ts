
interface Current {
	cloud: number;
	forecast:any;
	error: {
		message: string;
		code: number;
	}
}

export const fetchWeatherInfo = async (city: string): Promise<Current> => {
	const key:string = process.env.REACT_APP_API_KEY || ''
	const hour = new Date().getHours()
	const api:string = `https://api.weatherapi.com/v1/forecast.json?key=${key}&q=${city}&hour=${hour}&days=5&aqi=no&alerts=no`
	const { current, forecast, error } = await fetch(api)
		.then(res => res.json())
		.then(response => response)
		.catch(error => error)

		return { cloud: current?.cloud, error, forecast }
}

