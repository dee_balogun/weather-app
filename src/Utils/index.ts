export const Cities = ['Tokyo', 'Istanbul', 'Lagos']

export const getCurrentDay = (date: string): string => {
  return new Date(date).toDateString().split(' ')[0]
}
