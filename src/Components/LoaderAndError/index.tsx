import * as React from 'react'
import './index.less'

interface ErrorI {
    code: number;
    message: string
}

export const Loader: React.FC = () => <p className='loading'>Fetching Data...</p>

export const Error: React.FC<ErrorI> = ({ code, message, }) => {

    return (
        <p className='error'>{code === 1006 
            ? message
                : 'We cannot fetch the weather right now.'}</p>
    )
}
