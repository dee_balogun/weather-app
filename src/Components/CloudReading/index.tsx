import * as React from 'react'
import './index.less'

interface State {
	
}
interface Props { cloud: number, small?: boolean | false }

class CloudReading extends React.Component<Props, State> {


	render(): React.ReactNode {
		const { cloud, small } = this.props
		
			return (
				<div className='temp-clouds'>
					<p className={ small ? 'cloud-small': 'cloud-large'}>{cloud || 0}
					<sup className={ small ? 'small': 'large'}>o</sup>
					</p>
				</div>
			)
	}

}

export default CloudReading