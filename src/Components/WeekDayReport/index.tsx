import * as React from 'react'
import CloudReading from '../CloudReading'
import './index.less'
import RAINFALL from '../../Assets/images/rainFall.svg'
import CLOUDS from '../../Assets/images/clouds.png'

  interface Props {
		day: string;
		cloud: number;
		willItRain: boolean;
	}

class WeekDayReport extends React.Component<Props, {}> {

	render(): React.ReactNode {

		const { cloud, day, willItRain } = this.props
			return (
				<article className='day'>
					<h5>{day}</h5>
					<img src={willItRain
							? RAINFALL : CLOUDS}
						className='svg' alt='Clouds'/>
					<CloudReading
						small
						cloud={cloud}
					/>
					</article>
				)
		}
}
export default WeekDayReport
