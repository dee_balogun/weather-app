import * as React from 'react'

import * as api from './Api'
import './App.less'
import CLOUDS from './Assets/images/clouds.png'
import CloudReading from './Components/CloudReading';
import {Loader, Error } from './Components/LoaderAndError';
import WeekDayReport from './Components/WeekDayReport';
import { Cities, getCurrentDay } from './Utils';

interface State {
	cloud: number;
	selectedCity: string;
	forecast: Array<{
		date: string; 
		hour: Array<{ 
				cloud: number, 
				will_it_rain: boolean 
			}>
		}>;
	previousRequestHour: number
	error: {
		code: number;
		message: string;
	}
}

interface Props {}

class App extends React.Component<Props, State> {

	interval: number | undefined
	delayTime: number = 59 // delay for 59 minutes: 1 hour real time
	state: State = {
		cloud: 0,
		forecast: [],
		previousRequestHour: new Date().getUTCHours(),
		selectedCity: Cities[0],
		error: { code: 0, message: ''}
	}

	
	async componentDidMount() {

		const { selectedCity } = this.state

		await this.fetchWeatherInfo(selectedCity)
		await this.setHourlyRequest(selectedCity)
	
	}

	/**
	 *  Fetch weather info
	 * @param city string
	 */
	fetchWeatherInfo = async (city: string) => {
		const { cloud, error, forecast } = await api.fetchWeatherInfo(city)
		this.setState({
			cloud, 
			error,
			forecast: forecast.forecastday.splice(1)
		})
	}


	/**
	 *  Gets the selected city - onClick Handler
	 * @param city string
	 */
	getSelectedCity = (city: string) => {
		this.clearSetInterval()

		// We want to make calls every hour and for different cities
		// if within same hour and same city, no call is made
		if (this.state.previousRequestHour === new Date().getUTCHours() 
			&& city !== this.state.selectedCity) 
			{
				this.setState({ selectedCity: city })
				this.fetchWeatherInfo(city)
				this.setHourlyRequest(city)
		}
	}


	/**
	 *  This function is executed every minute
	 *  
	 * @param selectedCity 
	 */
	setHourlyRequest = async (selectedCity: string) => {
		this.interval = window.setInterval(async () => {
			if (new Date().getMinutes() === this.delayTime) {
				await this.fetchWeatherInfo(selectedCity)
			} 
			this.clearSetInterval()
			this.setHourlyRequest(selectedCity)
		}, 60000)
	}


clearSetInterval = () => {
	clearInterval(this.interval)
	this.interval = undefined
}

	componentWillUnmount(): void {
		clearInterval(this.interval)
	}

	render(): React.ReactNode {

		const { cloud, error, selectedCity, forecast } = this.state

		return(
			<React.StrictMode>
				<main className='container'>
					<section className='cities-wrapper'>
						{Cities.map(city =>{
							return(
								<button
									data-active={city === selectedCity}
									key={city}
									onClick={() => this.getSelectedCity(city)}>{city}
								</button>
							)
						})}
					</section>

					{Boolean(error?.code) && <Error code={error?.code} message={error.message}/>}

					{ !cloud && !forecast.length && <Loader/>}

					<section className='report-summary'>
						<section className='report-summary-inner-wrapper'>
							<h1>Today</h1>
							<section className='temperature-container'>
								<img src={CLOUDS} className='svg' alt='Clouds'/>
									<div>
										<CloudReading cloud={cloud} />
										<p className='clouds' >Clouds</p>
									</div>
							</section>
		
						</section>
						<section className='temperature-per-day'>
							{forecast.map((day, index) => {
									const currentDay = getCurrentDay(day.date)
									const cloud = day.hour[0].cloud
									const willItRain = day.hour[0].will_it_rain
								return(
									<WeekDayReport
										key={index}
										day={currentDay}
										cloud={cloud}
										willItRain={willItRain}
									/>
								)
							})}
						</section>
					</section>
				</main>
			</React.StrictMode>
		)
	}
}

export default App
