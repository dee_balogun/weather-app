# Weather-app


## APP Running Live

- Hosted on Netlify - https://weather-app-dee.netlify.app/

## API for weather 

- forecast api - https://www.weatherapi.com/api-explorer.aspx#current

## Considerations and Assumptions

- Real time data: The api used to fetch the weather information updates its API every hour. This is considered the real time data in this case
- There's an assumption (from the design) that rainy day should be shown, thus a condition from the API `will_it_rain` has been used to control the display of image to show
- Responsive design was not given however, the app has been made responsive based on intuition.
- I also assume that for each that's not currently today, we want to display the weather at the current hour of that day. 

## Futre Enhancements 

- If i had time, i would write tests for every component using jest.
- I will clean up the code as there are currently some esLint errors that time did not permit me to fix

## Project Set up

- React
- Typescript
- Webpack
- esLint

## Test
Use the built-in continuous integration in GitLab.
- Clone the app
- Install deps.
- Run the app.
- or visit ------ for the hosted application.

## Environment Variables
 - REACT_APP_API_KEY={YourAPIKEY}
